renderDSSP = function (dssp) {
    var contentHTML = "";

    dssp.forEach((sp) => {
        const divItem = document.createElement('card-list');
        divItem.innerHTML = `
        <div class="card">
          <img src="${sp.img}" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">${sp.name}</h5>
            <p class="card-text">
             ${sp.desc}
            </p>
          </div> `
    });
};


function batLoading() {
    document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
    document.getElementById("loading").style.display = "none";
}